import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LoginComponent } from './componentes/acceso/login/login.component';
import { RegistrarComponent } from './componentes/acceso/registrar/registrar.component';
import { FormsModule } from '@angular/forms';
import { AgregarProductosComponent } from './componentes/producto/agregar-productos/agregar-productos.component';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { environment } from 'src/environments/environment';
import { ListarProductosComponent } from './componentes/producto/listar-productos/listar-productos.component';
import { AgregarVentaComponent } from './componentes/venta/agregar-venta/agregar-venta.component';
import { HttpClientModule } from '@angular/common/http';
import { MenuComponent } from './componentes/menu/menu.component';
import { ListarVentasComponent } from './componentes/venta/listar-ventas/listar-ventas.component';
import { FbdatePipe } from './pipes/fbdate.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrarComponent,
    AgregarProductosComponent,
    ListarProductosComponent,
    ListarVentasComponent,
    AgregarVentaComponent,
    MenuComponent,
    FbdatePipe,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig, 'ventasApp'),
    AngularFirestoreModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
